import React from 'react';
import Form from './form'
import { StyleSheet, Text, View, ListView, Image } from 'react-native';
const ds=undefined;
export default class ListaPets extends React.Component {
  constructor(props){
    super(props);
    ds = new ListView.DataSource({rowHasChanged:(r1,r2)=> r1 !== r2})
    this.state = {
      zipcode:undefined,
      animal:undefined,
      dados:[],
      dataSource:ds.cloneWithRows([])
    }
  }

  search = (zipcode, animal) =>{
    fetch(`http://api.petfinder.com/pet.find?format=json&animal=${animal}&location=${zipcode}&key=e8399e728f691a086c1769fd314bf83e`)
     .then(res => res.json())
     .then(resp => {
       this.setState({dados:[]});
       let pets = resp.petfinder.pets.pet;
       let ar = [];
        for(pet of pets){
          let temp = {
            nome:pet.name["$t"],
            sexo:pet.sex["$t"],
            idade:pet.age["$t"]
          }
          if(pet.media.photos){
            let foto = pet.media.photos.photo[2];
            temp["imagem"] = foto["$t"];
          }
          ar.push(temp);
        }
        this.setState({dataSource:ds.cloneWithRows(ar)});
     })
     .catch(e => {throw(e)});
  }

  lista(dados){
    return(
      <View style={styles.listItem}>
        <View style={styles.w40}>
          <Image style={styles.image} 
            source={{uri:dados.imagem}}/>
        </View>
        <View style={styles.w60}>
          <Text style={styles.title}>{dados.nome}</Text>
          <Text >{dados.idade}</Text>
          <Text >{dados.sexo}</Text>
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <Form search={this.search} />
        <ListView
        enableEmptySections={true} 
        dataSource = {this.state.dataSource}
        renderRow={this.lista}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    height:'100%',
    width:'100%'
  },
  listItem:{
      borderBottomWidth:1,
      borderBottomColor:"#000",
      marginTop:10,
      display:'flex',
      flexDirection:"row",
      flexWrap:"wrap",
  },
  w40: {
    width:'30%',
    padding:10
  },
  w60:{
      width:"70%",
      padding:30
  },
  image:{
    width:100,
    height:100,
    borderRadius:100,
    borderWidth:3,
    borderColor:"#841584"
  },
  title:{
    textAlign:"center",
    fontSize:15,
    fontWeight:"bold"
  }
});
