import React from 'react';
import { AppRegistry ,
    StyleSheet, 
    Text, 
    View,
    TextInput,
    Picker,
    Button } from 'react-native';

export default class Form extends React.Component {

  constructor(props){
    super(props);
    this.state = {
        picker:"cat",
        input:"90001"
    }
  }

  buttonPressed = () =>{
    this.props.search(this.state.input, this.state.picker);
  }

  render() {
    return (
    <View>
        <View style={styles.container}>
          <Picker
              selectedValue={this.state.picker}
              style={styles.w40}
              onValueChange={(itemValue, itemIndex)   =>this.setState({picker:itemValue})}
          >
              <Picker.item label="Cat" value="cat"/>
              <Picker.item label="Dog" value="dog"/>
              <Picker.item label="Bird" value="bird"/>
          </Picker>
          <TextInput style={styles.input}
          value={this.state.input} 
          onChangeText={(text)=>this.setState({input:text})}
          selectionColor="#841584"
          />
          
        </View>
        <View style={styles.buttonDiv}>
            <Button 
                  title="Search"
                  color="#841584"
                  accessibilityLabel="Button for Search"
                  onPress={this.buttonPressed}
                  style={styles.button}
              />
        </View>
        
    </View>
    );
  }
}

const styles = StyleSheet.create({
  w40: {
    width:'30%',
    color:"#841584",
  },
  input:{
      height:40,
      width:"70%",
      color:"#841584"
  },
  container:{
      marginTop:40,
      display:'flex',
      flexDirection:"row",
      flexWrap:"wrap",
  },
  button:{
      width:200,
      textAlign:"center"
  },
  buttonDiv:{
    display:'flex',
    width:"100%",
    paddingRight:15,
    paddingBottom:10,
    alignItems:"flex-end",
    borderBottomWidth: 1,
    borderBottomColor:"#000"
  }
});

AppRegistry.registerComponent('Form', ()=>Form);